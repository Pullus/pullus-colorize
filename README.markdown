# Colorize 06.04.2022

`Colorize` is command line tool to colorize text from the standard input (stdin).

Based on a rulebook each line of the standard input is colorized by adding ANSI escape codes and print the colored line to the standard output.


### Usage

The output of a `command` is forwarded (piped) to `Colorize`

	<command> | colorize <file name of a rulebook>

that uses a rulebook (defined in `<file name of a rulebook>`) .

`Colorize` applies all rules from the rulebook to every line and prints the result to the standard output (stdout) to coloring the input lines.


### Example

To colorize this readme file, call `colorize` with the example rulebook `Examples/markdown.json`

```
cat README.markdown | Sources/.build/release/colorize Examples/markdown.json
```

The rulebook contains some rules to hightlight headers, lists, code, bold and TODO annotations.

![Colorized ReadMe](./Documentation/ReadMe-colorized.png)

## Build

The application can be build with the Swift Package Manager. Simply call

```
cd Sources
swift build -c release
```
	
to compile `colorize`. The binary will be generated at `.build/release/colorize`.



## Rulebook

The rulebook is a JSON or YAML file that contains one or more rules to colorize a single line.

### YAML rulebook example

```
name: Introduction example
rules:
  - description: Highlight https... -- not the address
    selector: 'http[s]:'
    style:
      underline: single
  - description: Highlight TODO
    selector: 'TODO:'
    style:
      attributes: slow blink
      color: red
```

and the same with JSON

### JSON rulebook example

```
{
   "name" : "Introduction example",
  "rules" : [
    {
    "description" : "Highlight https... -- not the address",
       "selector" : "http[s]:",
          "style" : { "underline": "single" }
    },
    {
    "description" : "Highlight TODO",
       "selector" : "TODO:",
          "style" : { "attributes": "slow blink", "color": "red" }
    }  ]
}
```

More examples are located at the `examples` directory.

### Syntax: Rulebook

 -  `name` (optional)

	A rulebook has an optional `name`. It is ignored by `colorize` and can be used to describe the rules.

 - `rules` (mandotory)

	The `rules` is an ordered list of rules. They are applied to each input line in the order of their appearance.
	
### Syntax: Rule

 - `description` (optional)

 	A human readable description. Can be used to explain the selector and style(s). It is ignored by `colorize`.
 	
 - `selector` (mandotory)

	A regular expression for selecting single or multiple parts of a single line or for selecting the entire line. The selector is evaluated with [NSRegularExpression](https://developer.apple.com/documentation/foundation/nsregularexpression).
	
	An expression can contain individual capture groups that can be colored with different styles. See style(s)
	
	_Hint: JSON use the backslash as an escape character. To use a backslash within a regular expression the backslash must be escaped as "`\\`". E.g. The regular expression "`\d`" must be typed as "`\\d`" within JSON._
 
 - `style` (mandotory)

 	A style is used to format a selected part. It is mandatory but can be empty.
 	
 	If the selector has no groups this style is used to format every match. Otherwise it is the default style for all groups.

 - `styles` (optional)

	A list of styles to format the matching groups of the selector. The first style is used for the first group and so on.
	
	If the selector has no groups the `styles` are ignored. If a style
     - is missing (the selector has more groups than this list has styles),
     - is empty (no styling attribute is set) or
     - the entire `styles` is missing

	the mandatory `style` is used to format a matched group.
	
### Syntax: Style

All styling attributes are optional. **An empty style without any styling is allowed**.

 - `color` (optional)

	One of the predefined ANSI colors `black`, `red`, `green`, `yellow`, `blue`, `magenta`, `cyan`, `white`, `gray`, `brightred`, `brightgreen`, `brightyellow`, `brightblue`, `brightmagenta`, `brightcyan` and `brightwhite`.
 
 - `background` (optional)

	One of the predefined ANSI colors `black`, `red`, `green`, `yellow`, `blue`, `magenta`, `cyan`, `white`, `gray`, `brightred`, `brightgreen`, `brightyellow`, `brightblue`, `brightmagenta`, `brightcyan` and `brightwhite`.
	
 - `weight` (optional)

 	Possible values are `normal` and `bold`.
 	
 - `underline` (optional)

 	Possible values are `none`, `single`, `double`.
 
 - `attributes` (optional)

 	A subset of the Select Graphic Rendition (SGR) **symbolic** attributes or **numeric** codes separated with a semicolon.
 	Supported values are: `bold`, `faint`, `italic`, `underline`, `slow blink`, `rapid blink`, `invert`, `hide`, `strike`, `framed`, `encircled`, `superscript`, `subscript`.


Extensions
----------

`bin/colored` is a prototypical bash script that can be used to automatically select a rulebook.

The idea is to provide a set of rulebooks (at `~/.config/colored/`) and to select one rulebook based on the command name.

E.g.

```
colored my_command …
```

would look for a `my_command.json` rulebook.



Requirements
------------

This application was build with Swift 5.6 and the Swift Package Manager.


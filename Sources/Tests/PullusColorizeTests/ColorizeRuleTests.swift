import XCTest
@testable import PullusColorize

class ColorizeRuleTests : ColorizeRuleTestsBase {
	
	func check(_ aSelector: String,
			   _ aStyle: ANSIStyle, styles aStyles: [ANSIStyle]?,
			   _ aLine: String, _ anExpectedResult: String,
			   file: StaticString = #file, line: UInt = #line) {
		let rule = Rule(description: nil, selector: aSelector, style: aStyle, styles: aStyles)
		
		XCTAssertEqual(rule.apply(to: aLine), anExpectedResult, file: file, line: line)
	}
}

final class ColorizeBasicTests : ColorizeRuleTests {

	func testIdentiy() throws {
		check("",
			  ANSIStyle(.red), styles: nil,
			  "Hello Peter",
			  "Hello Peter")
	}
}

final class ColorizeNoGroupTests : ColorizeRuleTests {
	
	func test1Match() throws {
		check("A",
			  ANSIStyle(.green), styles: nil,
			  "start A end",
			  "start \(green)A\(reset) end")
	}
	
	func test2Matches() throws {
		check("A",
			  ANSIStyle(.green), styles: nil,
			  "start A center A end",
			  "start \(green)A\(reset) center \(green)A\(reset) end")
	}
	
	func test1OverlappingMatch() throws {
		check("AA",
			  ANSIStyle(.green), styles: nil,
			  "start AAA end",
			  "start \(green)AA\(reset)A end")
	}
	
	func test2OverlappingMatches() throws {
		check("AA",
			  ANSIStyle(.green), styles: nil,
			  "start AAA center AAA end",
			  "start \(green)AA\(reset)A center \(green)AA\(reset)A end")
	}
}

final class Colorize1GroupTests : ColorizeRuleTests {
	
	func testEmptyGroup() {
		check("(A)-(B)?",
			  ANSIStyle(.green), styles: nil,
			  "start A- end",
			  "start \(green)A\(reset)- end")
	}
	
	
	func test1Match() throws {
		check("(A)",
			  ANSIStyle(.green), styles: nil,
			  "start A end",
			  "start \(green)A\(reset) end")
	}
	
	func test2Matches() throws {
		check("(A)",
			  ANSIStyle(.green), styles: nil,
			  "start A center A end",
			  "start \(green)A\(reset) center \(green)A\(reset) end")
	}
	
	func test1OverlappingMatch() throws {
		check("(AA)",
			  ANSIStyle(.green), styles: nil,
			  "start AAA end",
			  "start \(green)AA\(reset)A end")
	}
	
	func test2OverlappingMatches() throws {
		check("(AA)",
			  ANSIStyle(.green), styles: nil,
			  "start AAA center AAA end",
			  "start \(green)AA\(reset)A center \(green)AA\(reset)A end")
	}
}

final class Colorize2GroupTests : ColorizeRuleTests {
	
	func test1Match() throws {
		check("(A)-(B)",
			  ANSIStyle(.green), styles: nil,
			  "start A-B end",
			  "start \(green)A\(reset)-\(green)B\(reset) end")
	}
	
	func test2Matches() throws {
		check("(A)-(B)",
			  ANSIStyle(.green), styles: nil,
			  "start A-B center A-B end",
			  "start \(green)A\(reset)-\(green)B\(reset) center \(green)A\(reset)-\(green)B\(reset) end")
	}
	
	func test1OverlappingMatch() throws {
		check("(A)-(A)",
			  ANSIStyle(.green), styles: nil,
			  "start A-A-A end",
			  "start \(green)A\(reset)-\(green)A\(reset)-A end")
	}

	func test2OverlappingMatches() throws {
		check("(A)-(A)",
			  ANSIStyle(.green), styles: nil,
			  "start A-A-A center A-A-A end",
			  "start \(green)A\(reset)-\(green)A\(reset)-A center \(green)A\(reset)-\(green)A\(reset)-A end")
	}
	
	func test1InterleavedMatches() throws {
		check("(A)...(B)",
			  ANSIStyle(.green), styles: nil,
			  "start A A B B end",
			  "start \(green)A\(reset) A \(green)B\(reset) B end")
	}
	
	func test2Colors1Match() {
		check("(A)-(B)",
			  ANSIStyle(.green), styles: [ANSIStyle(.blue), ANSIStyle(.red)],
			  "start A-B end",
			  "start \(blue)A\(reset)-\(red)B\(reset) end")
	}
	
	func testDefaultStyle() {
		check("(A)-(B)",
			  ANSIStyle(.green), styles: [ANSIStyle(), ANSIStyle(.red)],
			  "start A-B end",
			  "start \(green)A\(reset)-\(red)B\(reset) end")
	}
}

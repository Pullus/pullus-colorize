import XCTest
@testable import PullusColorize

class ColorizeRulebookJSONYAMLTests : ColorizeRuleTestsBase {

	let jsonPath = Bundle.module.path(forResource: "rulebook", ofType: "json")!
	let yamlPath = Bundle.module.path(forResource: "rulebook", ofType: "yaml")!

	func check(_ text: String,
			   _ anExpectedResult: String,
			   file: StaticString = #file, line: UInt = #line)
		throws {
			try XCTAssertEqual(Rulebook(path: yamlPath).apply(to: text), anExpectedResult)
			try XCTAssertEqual(Rulebook(path: jsonPath).apply(to: text), anExpectedResult)
	}
	
	func testANSIColor() throws {
		try check("red", red + "red" + reset)
	}
	
	func testANSIBackground() throws {
		try check("night", blackBackground + "night" + reset)
	}
	
	func testANSIWeight() throws {
		try check("bold", bold + "bold" + reset)
	}

	func testANSIUnderline() throws {
		try check("single underline", singleUnderline + "single underline" + reset)
	}
	
	func testANSISGRAttributes() throws {
		try check("slow blink", slowBlink + "slow blink" + reset)
	}
}

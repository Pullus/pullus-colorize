import XCTest
@testable import PullusColorize

class ColorizeRuleTestsBase : XCTestCase {

	let reset =				"\u{001B}[0m"

	let bold =				"\u{001B}[1m"
	let singleUnderline =	"\u{001B}[4m"
	let slowBlink =			"\u{001B}[5m"
	
	let black =				"\u{001B}[30m"
	let red =				"\u{001B}[31m"
	let green =				"\u{001B}[32m"
	let blue =				"\u{001B}[34m"

	let blackBackground =	"\u{001B}[40m"
}

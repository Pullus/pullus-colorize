// swift-tools-version:5.5

import PackageDescription

let package = Package(
	name: "PullusColorize",
	products: [
		.executable(
			name: "colorize",
			targets: ["PullusColorize"]),
		],
	dependencies: [
		.package(url: "https://github.com/apple/swift-argument-parser", from: "1.1.1"),
		.package(url: "https://github.com/jpsim/Yams.git", from: "5.0.0"),
		],
	targets: [
		.executableTarget(
			name: "PullusColorize",
			dependencies: [
				.product(name: "ArgumentParser", package: "swift-argument-parser"),
				.product(name: "Yams", package: "Yams"),
				]),
		.testTarget(
			name: "PullusColorizeTests",
			dependencies: ["PullusColorize"],
			resources: [
				.copy("rulebook.json"),
				.copy("rulebook.yaml"),
				]),
		]
)

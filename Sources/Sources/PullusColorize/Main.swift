import ArgumentParser

@main
class MainCommand : AsyncParsableCommand {
	
	@Argument(help: "Name of the rulebook file")
	var config: String
	
	required init() {}
	
	func run() async throws {
		do {
			let rulebook = try Rulebook(path: config)
	
			Colorize(rulebook: rulebook).go()
		} catch {
			print("Can't read rulebook file. Because \"\(error)\".")
		}
	}
}

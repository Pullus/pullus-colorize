extension Array where Element == ANSIStyle {
	
	///
	/// Returns the style at `anIndex` if it exists and is not empty (at least
	/// one styling attribute is set), otherwise `aDefault`.
	///
	func style(at anIndex: Int, or aDefault: ANSIStyle) -> Element {
		if isEmpty { return aDefault }
		if anIndex < startIndex || endIndex <= anIndex { return aDefault }
		
		let element = self[anIndex]
		
		// Check styling attributes
		if element.isEmpty { return aDefault }
		
		return element
	}
}

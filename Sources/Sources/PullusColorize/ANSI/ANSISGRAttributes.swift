import Foundation

///
/// Select Graphic Rendition (SGR) Attributes.
///
/// A generic styling to enable all SGR attributes.
///
/// See: https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
///
struct ANSISGRAttributes : ANSISelectGraphicRendition, Codable {
	
	///
	/// Symbolic names of SGR attributes and their codes.
	///
	static let codeTable: [String : UInt] = [
		"bold"			:  1,
		"faint"			:  2,
		"italic"		:  3,
		"underline"		:  4,
		"slow blink"	:  5,
		"rapid blink"	:  6,
		"invert"		:  7,
		"hide"			:  8,
		"strike"		:  9,
		"framed"		: 51,
		"encircled"		: 52,
		"superscript"	: 73,
		"subscript"		: 74,
	]
	
	///
	/// List of ANSI SGR attributes.
	///
	let codes: [UInt]
	
	///
	/// Creates the `codes` array by parsing a single string and split it at
	/// every `;`.
	///
	/// Possible components are SGR codes (numbers) or symbolic names. See
	/// `codeTable`.
	///
	init(from aDecoder: Decoder) throws {
		codes = try aDecoder
			.singleValueContainer()
			.decode(String.self)
			.components(separatedBy: ";")
			.map { UInt($0) ?? ANSISGRAttributes.codeTable[$0] }
			.compactMap { $0 }
	}
}

import Foundation

///
/// See: https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
///
enum ANSIBackground : UInt, ANSISelectGraphicRendition, CaseNameCodable {
	
	case black		= 40
	case red		= 41
	case green		= 42
	case yellow		= 43
	case blue		= 44
	case magenta	= 45
	case cyan		= 46
	case white		= 47
	
	case gray			= 100
	case brightred		= 101
	case brightgreen	= 102
	case brightyellow	= 103
	case brightblue		= 104
	case brightmagenta	= 105
	case brightcyan		= 106
	case brightwhite	= 107
	
	var codes: [UInt] { [rawValue] }
}

import Foundation

///
/// See: https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
///
enum ANSIFontWeight : UInt, ANSISelectGraphicRendition, CaseNameCodable {
	
	case normal	= 22
	case bold	=  1
	
	var codes: [UInt] { [rawValue] }
}

import Foundation

///
/// See: https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
///
protocol ANSISelectGraphicRendition {
	
	var codes: [UInt] { get }
}

extension ANSISelectGraphicRendition {
	
	var sequence: String {
		let codeSequence = codes.map { String($0) }.joined(separator: ";")
		
		return "\u{001B}[\(codeSequence)m"
	}
	
	func apply(_ text: String) -> String {
		return sequence + text + "\u{001B}[0m"
	}

}

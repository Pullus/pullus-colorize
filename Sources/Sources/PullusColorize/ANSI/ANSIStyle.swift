import Foundation

///
/// See: https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
///
struct ANSIStyle : Codable {
	
	let color: ANSIColor?
	let background: ANSIBackground?
	let weight: ANSIFontWeight?
	let underline: ANSIUnderline?
	let attributes: ANSISGRAttributes?
	
	var styles: [ANSISelectGraphicRendition]  {
		let optionalStyles: [ANSISelectGraphicRendition?] = [
			color,
			background,
			weight,
			underline,
			attributes]
		
		return optionalStyles.compactMap { $0 }
	}
	
	///
	/// True if no styling attribute is set for this style.
	///
	var isEmpty: Bool { color == nil && weight == nil && underline == nil && attributes == nil }
	
	init(_ aColor: ANSIColor? = nil, background aBackground: ANSIBackground? = nil, _ aWeight: ANSIFontWeight? = nil, _ anUnderline: ANSIUnderline? = nil, attributes anAttributes: ANSISGRAttributes? = nil) {
		color = aColor
		background = aBackground
		weight = aWeight
		underline = anUnderline
		attributes = anAttributes
	}
	
	func apply(to text: String) -> String {
		let codes = styles
			.flatMap { $0.codes }
			.map { String($0) }
			.joined(separator: ";")
		
		return "\u{001B}[\(codes)m\(text)\u{001B}[0m"
	}
}

import Foundation

///
/// See: https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
///
enum ANSIColor : UInt, ANSISelectGraphicRendition, CaseNameCodable {
	
	case black		= 30
	case red		= 31
	case green		= 32
	case yellow		= 33
	case blue		= 34
	case magenta	= 35
	case cyan		= 36
	case white		= 37
	
	case gray			= 90
	case brightred		= 91
	case brightgreen	= 92
	case brightyellow	= 93
	case brightblue		= 94
	case brightmagenta	= 95
	case brightcyan		= 96
	case brightwhite	= 97

	var codes: [UInt] { [rawValue] }
}

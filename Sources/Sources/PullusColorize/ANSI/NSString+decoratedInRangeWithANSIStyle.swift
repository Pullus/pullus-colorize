import Foundation

extension NSString {
	
	///
	/// Returns a (new) decorated string.
	///
	/// If the range is invalid (`NSNotFound`) `self` is returned. This could
	/// be a problem is `self` is a `NSMutableString`.
	///
	func decorated(in aRange: NSRange, with aStyle: ANSIStyle) -> NSString {
		// Skip empty groups
		guard aRange.location != NSNotFound else { return self }
		
		let selection = substring(with: aRange)
		let result = replacingCharacters(
			in: aRange,
			with: selection.decorated(with: aStyle))
		
		return NSString(string: result)
	}
	
}

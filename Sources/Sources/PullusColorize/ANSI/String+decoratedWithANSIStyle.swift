extension String {
	
	func decorated(with aStyle: ANSIStyle) -> String {
		return aStyle.apply(to: self)
	}
	
}

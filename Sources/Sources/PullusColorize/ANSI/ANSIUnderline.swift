import Foundation

///
/// See: https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
///
enum ANSIUnderline : UInt, ANSISelectGraphicRendition, CaseNameCodable {
	
	case none	= 24
	case single	=  4
	case double	= 21
	
	var codes: [UInt] { [rawValue] }
}

import Foundation

import Yams

struct Rulebook : Codable {

	let name: String?
	let rules: [Rule]
	
	///
	/// Path to a JSON or YAML file with an encoded rulebook.
	///
	init(path aPath: String) throws {
		let data = try String(contentsOfFile: aPath).data(using: .utf8) ?? Data()

		if aPath.hasSuffix(".json") {
			try self.init(json: data)
		} else {
			try self.init(yaml: data)
		}
	}
	
	init(json aData: Data) throws {
		self = try aData.decode(Rulebook.self, with: JSONDecoder())
	}
	
	init(yaml aData: Data) throws {
		self = try aData.decode(Rulebook.self, with: YAMLDecoder())
	}
	
	func apply(to aLine: String) -> String {
		return rules.reduce(aLine) { $1.apply(to: $0) }
	}
}

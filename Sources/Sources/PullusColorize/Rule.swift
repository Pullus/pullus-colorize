import Foundation

struct Rule : Codable {

	///
	/// A human readable description.
	///
	/// Usefull to explain the selector and style.
	///
	let description: String?
	
	///
	/// A regular expression for selecting single or multiple parts of a single
	/// line or for selecting the entire line.
	///
	let selector: String
	
	///
	/// A style to format a selected part.
	///
	/// If the selector has no groups this style is used to format every match.
	/// Otherwise it is the default style for all groups.
	///
	let style: ANSIStyle
	
	///
	/// A list of styles to format the matching groups of the selector.
	///
	/// If the selector has no groups the `styles` are ignored.
	///
	/// If a style
	///  - is missing (the selector has more groups than this list has
	///    styles),
	///  - the style is empty (no styling attribute is set) or
	///  - the entire `styles` is missing
	/// the mandatory `style` is used to format a matched group.
	///
	let styles: [ANSIStyle]?
	
	func apply(to aLine: String) -> String {
		guard
			let expression = try? NSRegularExpression(pattern: selector)
		else { return aLine }
		
		let input = NSString(string: aLine)
		let range = NSRange(location: 0, length: input.length)

		// The substitution of a match/group changes the length of the string
		// and so the ranges of the later matches no longer coincide with the
		// original text. Therefore the matches/groups are processed from the
		// last to the first.
		//
		// This nevertheless has some pitfalls for overlapping, nested or
		// interleaved matches.
		// Due to missing specifications of `NSRegulareExpression.matches` the
		// current behaviour is checked by some tests.
		let matches = expression
			.matches(in: input as String, range: range)
			.reversed()
		
		return expression.hasCaptureGroups
			? applyStyles(matches: matches, to: input) as String
			: applyStyle(matches: matches, to: input) as String
	}
	
	///
	/// Applies the `style` to ungrouped matches.
	///
	func applyStyle<COLLECTION : Collection>(matches: COLLECTION, to aText: NSString) -> NSString where COLLECTION.Element == NSTextCheckingResult {
		return matches.reduce(aText) { result, match in
			result.decorated(in: match.range, with: style)
		}
	}

	///
	/// Applies the `styles` to grouped matches.
	///
	func applyStyles<COLLECTION : Collection>(matches: COLLECTION, to aText: NSString) -> NSString where COLLECTION.Element == NSTextCheckingResult {
		// Set a default style
		let styles = styles ?? [style]
		
		var result = aText
		
		for match in matches {
			for index in stride(from: match.numberOfRanges - 1, to: 0, by: -1) {
				result = result.decorated(
					in: match.range(at: index),
					with: styles.style(at: index - 1, or: style))
			}
		}
		
		return result
	}
}

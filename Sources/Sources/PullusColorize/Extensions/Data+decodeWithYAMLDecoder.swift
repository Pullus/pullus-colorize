import Foundation

import Yams
extension Data {
	
	public
	func decode<Result : Decodable>(_ aResultType: Result.Type, with aDecoder: YAMLDecoder) throws -> Result {
		return try aDecoder.decode(aResultType, from: self)
	}
}

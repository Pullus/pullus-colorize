import Foundation

extension Data {
	
	public
	func decode<Result : Decodable>(_ aResultType: Result.Type, with aDecoder: JSONDecoder) throws -> Result {
		return try aDecoder.decode(aResultType, from: self)
	}
	
}

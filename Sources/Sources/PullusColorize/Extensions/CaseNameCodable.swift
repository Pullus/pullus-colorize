import Foundation

///
/// Decodes an `RawRepresentable` `enum` by its `case` name an not by its
/// `rawValue`.
///
protocol CaseNameCodable : Codable, RawRepresentable & CaseIterable { }

extension CaseNameCodable {
	
	private
	var name: String { "\(self)" }
	
	init(from aDecoder: Decoder) throws {
		let container = try aDecoder.singleValueContainer()
		let decodedName = try container.decode(String.self)
		
		guard
			let decodedCase = Self.allCases.first(where: { $0.name == decodedName } )
		else { throw DecodingError.typeMismatch(
			Self.self,
			.init(codingPath: container.codingPath,
				  debugDescription: "No enum case can be decoded for \"\(decodedName)\"."))
		}
		
		self = decodedCase
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		
		try container.encode(self.name)
	}
}

import Foundation

extension NSRegularExpression {
	
	var hasCaptureGroups: Bool { numberOfCaptureGroups > 0 }
	
}

import Foundation

public
struct Colorize {
	let rulebook: Rulebook
	
	func go() {
		while true {
			guard let line = readLine() else { break }
			
			print(rulebook.apply(to: line))
		}
	}
}
